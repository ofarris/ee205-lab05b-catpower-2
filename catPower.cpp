///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date   01_02_2022
///////////////////////////////////////////////////////////////////////////////





#define DEBUG 
#include <stdio.h>
#include <stdlib.h>
#include "ev.h"
#include "foe.h"
#include "gge.h"
#include "megaton.h"
#include "cat.h"

int validEntry( int argc ) {
   if( argc != 4) {
      printf( "Energy converter\n" );
      printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
      printf( "   fromValue: A number that we want to convert\n" );
      printf( "   fromUnit:  The energy unit fromValue is in\n" );
      printf( "   toUnit:  The energy unit to convert to\n" );
      printf( "\n" );
      printf( "This program converts energy from one energy unit to another.\n" );
      printf( "The units it can convert are: \n" );
      printf( "   j = Joule\n" );
      printf( "   e = eV = electronVolt\n" );
      printf( "   m = MT = megaton of TNT\n" );
      printf( "   g = GGE = gasoline gallon equivalent\n" );
      printf( "   f = foe = the amount of energy produced by a supernova\n" );
      printf( "   c = catPower = like horsePower, but for cats\n" );
      printf( "\n" );
      printf( "To convert from one energy unit to another, enter a number \n" );
      printf( "it's unit and then a unit to convert it to.  For example, to\n" );
      printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
      printf( "\n" );
      return 0;
   }
   return 1;
}

int main( int argc, char* argv[] ) {
   double fromValue;
   char   fromUnit;
   char   toUnit;

   if(validEntry(argc) == 0){
      return 0;
   }

   
      fromValue = atof( argv[1] );
      fromUnit = argv[2][0];         // Get the first character from the second argument
      toUnit = argv[3][0];           // Get the first character from the thrid argument

   #ifdef DEBUG
      printf( "fromValue = [%lG]\n", fromValue );
      printf( "fromUnit = [%c]\n", fromUnit );
      printf( "toUnit = [%c]\n", toUnit );
   #endif

   int endProgram = 0;
   double commonValue;
   switch( fromUnit ) {
      case JOULE                    : commonValue = fromValue; // No conversion necessary
                                       break;
      case ELECTRON_VOLT            : commonValue = fromElectronVoltsToJoule( fromValue );
                                       break;
      case MEGATON                  : commonValue = fromMegatonToJoule( fromValue );
                                       break;
      case GASOLINEGALLONEQUIVALENT : commonValue = fromGasolineGallonEquivalentToJoule( fromValue );
                                       break;
      case FOE                      : commonValue = fromFoeToJoule( fromValue);
                                       break;
      case CATPOWER                 : commonValue = fromCatPowertoJoule();
                                       break;
      default                       :
                                       printf("Unknown fromUnit %c\n", fromUnit);
                                       endProgram = 1;
                                       break;
   }
   
   double toValue;
   switch( toUnit )  {
      case JOULE                    : toValue = commonValue;
                                       break;
      case ELECTRON_VOLT            : toValue = fromJouleToElectronVolts( commonValue );
                                       break;
      case MEGATON                  : toValue = fromJouleToMegaton( commonValue );
                                       break;
      case GASOLINEGALLONEQUIVALENT : toValue = fromJouleToGasolineGallonEquivalent( commonValue );
                                       break;
      case FOE                      : toValue = fromJouleToFoe( commonValue );
                                       break;
      case CATPOWER                 : toValue = fromJouleToCatPower();
                                       break;
      default                       :  printf("Unknown toUnit %c\n", toUnit);
                                       endProgram = 1;
                                        break;

   }
   if(endProgram == 1) {
      return 0;
   }
   
   #ifdef DEBUG 
      printf( "commonValue = [%lG] joule\n", commonValue );
   #endif

   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}

