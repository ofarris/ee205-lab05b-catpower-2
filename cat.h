/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
const char CATPOWER                 = 'c';
const double CATPOWER_IN_A_JOULE = 0;

extern double fromCatPowertoJoule();
   
extern double fromJouleToCatPower();
