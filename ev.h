/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.h
/// @version 1.0
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once

const char JOULE                    = 'j';

const char ELECTRON_VOLT            = 'e';
const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;

extern double fromElectronVoltsToJoule( double electronVolts );

extern double fromJouleToElectronVolts( double joule );
   


