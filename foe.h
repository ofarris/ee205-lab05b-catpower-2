/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
const char FOE                      = 'f';
const double FOE_IN_A_JOULE = 1/(1.0e24);


extern double fromFoeToJoule( double foe );
   
extern double fromJouleToFoe( double joule );
