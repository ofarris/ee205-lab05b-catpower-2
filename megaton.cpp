/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file metagon.cpp
/// @version 1.0
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "megaton.h"


double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATON_IN_A_JOULE;
}

double fromJouleToMegaton( double joule ) {
   return joule * MEGATON_IN_A_JOULE;
}

