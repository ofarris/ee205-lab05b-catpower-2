/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.h
/// @version 1.0
///
/// @author Oze Farris <ofarris@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
const char MEGATON                  = 'm';
const double MEGATON_IN_A_JOULE = 1/(4.184e15);

extern double fromMegatonToJoule( double megaton );

extern double fromJouleToMegaton( double joule );


